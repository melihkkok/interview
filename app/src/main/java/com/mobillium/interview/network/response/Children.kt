package com.mobillium.interview.network.response


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Children(
    @SerializedName("id")
    var id: Int? = null,
    @SerializedName("name")
    var name: String? = null,
    @SerializedName("parent_id")
    var parentId: Int? = null,
    @SerializedName("order")
    var order: Int? = null,
    @SerializedName("parent_category")
    var parentCategory: ParentCategory? = null,
    @SerializedName("logo")
    var logo: Logo? = null,
    @SerializedName("cover")
    var cover: Cover? = null,
    @SerializedName("children")
    var children: List<String>? = null
):Parcelable