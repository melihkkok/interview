package com.mobillium.interview.network.service

import com.mobillium.interview.network.response.ServiceResponse
import com.mobillium.interview.network.response.ServiceResponseItem
import io.reactivex.Single
import retrofit2.http.GET

interface ApiService {

    @GET("api/v2/discover")
    fun getProducts(): Single<ArrayList<ServiceResponseItem>>
}