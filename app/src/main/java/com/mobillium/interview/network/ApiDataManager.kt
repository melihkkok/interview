package com.mobillium.interview.network

import com.mobillium.interview.network.response.ServiceResponse
import com.mobillium.interview.network.response.ServiceResponseItem
import com.mobillium.interview.network.service.ApiService
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ApiDataManager @Inject constructor(private val apiService: ApiService){

    fun getProducts(): Single<ArrayList<ServiceResponseItem>> {
        return apiService.getProducts().applySchedulers()
    }

    fun <T> Single<T>.applySchedulers(): Single<T> {
        return subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
    }
}