package com.mobillium.interview.network.response


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Featured(
    @SerializedName("id")
    var id: Int? = null,
    @SerializedName("type")
    var type: String? = null,
    @SerializedName("cover")
    var cover: Cover? = null,
    @SerializedName("title")
    var title: String? = null,
    @SerializedName("sub_title")
    var subTitle: String? = null
):Parcelable