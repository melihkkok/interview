package com.mobillium.interview.network.response


import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class ServiceResponse(var serviceArray: ArrayList<ServiceResponseItem>) : Parcelable