package com.mobillium.interview.network.response.enums

enum class ListingType(val type: String) {
    TYPE_FEATURED("featured"),
    TYPE_NEW_PRODUCTS("new_products"),
    TYPE_CATEGORIES("categories"),
    TYPE_COLLECTIONS("collections"),
    TYPE_EDITOR_SHOPS("editor_shops"),
    TYPE_NEW_SHOPS("new_shops")
}