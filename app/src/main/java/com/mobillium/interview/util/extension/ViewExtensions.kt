package com.mobillium.interview.util.extension

import android.content.Context
import android.util.AttributeSet
import android.view.View
import androidx.core.view.isVisible
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import androidx.viewpager.widget.ViewPager

inline infix fun SwipeRefreshLayout.onSwipe(crossinline block: () -> Unit) {
    this.setOnRefreshListener {
        block()
        isRefreshing = false
    }
}