package com.mobillium.interview.util

import android.app.Activity
import androidx.core.app.ShareCompat

class AppUtils {

    companion object {

        fun shareToOutApps(activity: Activity, sharingUrl: String) {
            shareOtherApps(activity, sharingUrl)
        }

        private fun shareOtherApps(activity: Activity, sharingUrl: String) {
            ShareCompat.IntentBuilder
                .from(activity)
                .setType("text/plain")
                .setChooserTitle("Şununla paylaş :")
                .setText(sharingUrl)
                .startChooser()
        }
    }
}