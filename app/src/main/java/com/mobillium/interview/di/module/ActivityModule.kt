package com.mobillium.interview.di.module

import com.mobillium.interview.ui.collections.CollectionDetailActivity
import com.mobillium.interview.ui.home.HomeActivity
import com.mobillium.interview.ui.products.ProductsActivity
import com.mobillium.interview.ui.shop_detail.ShopDetailActivity
import com.mobillium.interview.ui.splash.SplashActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityModule {

    @ContributesAndroidInjector
    abstract fun bindSplashActivity(): SplashActivity

    @ContributesAndroidInjector
    abstract fun bindHomeActivity(): HomeActivity

    @ContributesAndroidInjector
    abstract fun bindProductsActivity(): ProductsActivity

    @ContributesAndroidInjector
    abstract fun bindCollectionDetailActivity(): CollectionDetailActivity

    @ContributesAndroidInjector
    abstract fun bindShopDetailActivity(): ShopDetailActivity

}