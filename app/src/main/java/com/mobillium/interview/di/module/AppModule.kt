package com.mobillium.interview.di.module

import dagger.Module

@Module(
    includes = [
        ViewModelModule::class
    ]
)
class AppModule {


}