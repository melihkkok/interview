package com.mobillium.interview.di.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.mobillium.interview.di.ViewModelFactory
import com.mobillium.interview.di.key.ViewModelKey
import com.mobillium.interview.ui.collections.CollectionDetailViewModel
import com.mobillium.interview.ui.home.HomeViewModel
import com.mobillium.interview.ui.products.ProductsViewModel
import com.mobillium.interview.ui.shop_detail.ShopDetailViewModel
import com.mobillium.interview.ui.splash.SplashViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {

    @Binds
    abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(SplashViewModel::class)
    abstract fun bindsSplashViewModel(viewModel: SplashViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(HomeViewModel::class)
    abstract fun bindsHomeViewModel(viewModel: HomeViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ProductsViewModel::class)
    abstract fun bindsProductsViewModel(viewModel: ProductsViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(CollectionDetailViewModel::class)
    abstract fun bindsCollectionDetailViewModel(viewModel: CollectionDetailViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ShopDetailViewModel::class)
    abstract fun bindsShopDetailViewModel(viewModel: ShopDetailViewModel): ViewModel
}