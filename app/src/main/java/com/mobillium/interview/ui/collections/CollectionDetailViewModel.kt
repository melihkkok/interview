package com.mobillium.interview.ui.collections

import android.os.Bundle
import com.mobillium.interview.base.viewmodel.BaseViewModel
import com.mobillium.interview.network.response.ServiceResponseItem
import javax.inject.Inject

class CollectionDetailViewModel @Inject constructor() : BaseViewModel(){

    var serviceResponse : ArrayList<ServiceResponseItem>? = null
    override fun handleIntent(extras: Bundle) {
        serviceResponse = extras.getParcelableArrayList(CollectionDetailActivity.EXTRA_SERVICE_ITEM) ?: arrayListOf()
    }
}