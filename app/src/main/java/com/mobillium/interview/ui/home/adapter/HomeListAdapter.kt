package com.mobillium.interview.ui.home.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import com.mobillium.interview.R
import com.mobillium.interview.base.adapter.BaseViewHolder
import com.mobillium.interview.databinding.CellEditorShopsViewPagerItemBinding
import com.mobillium.interview.databinding.CellFeaturedItemBinding
import com.mobillium.interview.databinding.CellHorizantalViewBinding
import com.mobillium.interview.network.response.ServiceResponseItem
import com.mobillium.interview.ui.home.view_holder.*

class HomeListAdapter constructor(
    private val serviceResponseItem: ArrayList<ServiceResponseItem>
    /*private val featuredList: ArrayList<Featured>,
    private val productsList: ArrayList<Product>,
    private val categoriesList: ArrayList<Category>,
    private val collectionsList: ArrayList<Collection>,
    private val shopsList: ArrayList<Shop>*/
) : RecyclerView.Adapter<BaseViewHolder<*>>() {

    val liveData = MutableLiveData<State>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<*> {
        val layoutInflater = LayoutInflater.from(parent.context)
        return HomeItemType.values()[viewType].onCreateViewHolder(
            parent,
            layoutInflater,
            liveData
        )

    }

    override fun getItemViewType(position: Int): Int {
        return when (position) {
            0 -> HomeItemType.TYPE_FEATURED.viewType()
            1 -> HomeItemType.TYPE_PRODUCT.viewType()
            2 -> HomeItemType.TYPE_CATEGORIES.viewType()
            3 -> HomeItemType.TYPE_COLLECTIONS.viewType()
            4 -> HomeItemType.TYPE_EDITOR_SHOPS.viewType()
            else -> HomeItemType.TYPE_NEW_SHOPS.viewType()
        }
    }

    override fun getItemCount(): Int {
        return serviceResponseItem.size
    }

    override fun onBindViewHolder(holder: BaseViewHolder<*>, position: Int) {
        when (holder) {
            is FeaturedViewHolder -> holder.bind(serviceResponseItem)
            is ProductsViewHolder -> holder.bind(serviceResponseItem)
            is CategoriesViewHolder -> holder.bind(serviceResponseItem)
            is CollectionsViewHolder -> holder.bind(serviceResponseItem)
            is EditorShopsViewHolder -> holder.bind(serviceResponseItem)
            is NewShopsViewHolder -> holder.bind(serviceResponseItem)
        }
    }

    enum class HomeItemType {
        TYPE_FEATURED {
            override fun onCreateViewHolder(
                parent: ViewGroup,
                layoutInflater: LayoutInflater,
                liveData: MutableLiveData<State>
            ): BaseViewHolder<*> {
                val binding = DataBindingUtil.inflate<CellFeaturedItemBinding>(
                    layoutInflater,
                    R.layout.cell_featured_item,
                    parent,
                    false
                )
                return FeaturedViewHolder(binding)
            }
        },
        TYPE_PRODUCT {
            override fun onCreateViewHolder(
                parent: ViewGroup,
                layoutInflater: LayoutInflater,
                liveData: MutableLiveData<State>
            ): BaseViewHolder<*> {
                val binding = DataBindingUtil.inflate<CellHorizantalViewBinding>(
                    layoutInflater,
                    R.layout.cell_horizantal_view,
                    parent,
                    false
                )
                return ProductsViewHolder(binding, liveData)
            }
        },
        TYPE_CATEGORIES {
            override fun onCreateViewHolder(
                parent: ViewGroup,
                layoutInflater: LayoutInflater,
                liveData: MutableLiveData<State>
            ): BaseViewHolder<*> {
                val binding = DataBindingUtil.inflate<CellHorizantalViewBinding>(
                    layoutInflater,
                    R.layout.cell_horizantal_view,
                    parent,
                    false
                )
                return CategoriesViewHolder(binding)
            }
        },
        TYPE_COLLECTIONS {
            override fun onCreateViewHolder(
                parent: ViewGroup,
                layoutInflater: LayoutInflater,
                liveData: MutableLiveData<State>
            ): BaseViewHolder<*> {
                val binding = DataBindingUtil.inflate<CellHorizantalViewBinding>(
                    layoutInflater,
                    R.layout.cell_horizantal_view,
                    parent,
                    false
                )
                return CollectionsViewHolder(binding, liveData)
            }
        },
        TYPE_EDITOR_SHOPS {
            override fun onCreateViewHolder(
                parent: ViewGroup,
                layoutInflater: LayoutInflater,
                liveData: MutableLiveData<State>
            ): BaseViewHolder<*> {
                val binding = DataBindingUtil.inflate<CellEditorShopsViewPagerItemBinding>(
                    layoutInflater,
                    R.layout.cell_editor_shops_view_pager_item,
                    parent,
                    false
                )
                return EditorShopsViewHolder(binding, liveData)
            }
        },
        TYPE_NEW_SHOPS {
            override fun onCreateViewHolder(
                parent: ViewGroup,
                layoutInflater: LayoutInflater,
                liveData: MutableLiveData<State>
            ): BaseViewHolder<*> {
                val binding = DataBindingUtil.inflate<CellHorizantalViewBinding>(
                    layoutInflater,
                    R.layout.cell_horizantal_view,
                    parent,
                    false
                )
                return NewShopsViewHolder(binding,liveData)
            }
        };

        abstract fun onCreateViewHolder(
            parent: ViewGroup,
            layoutInflater: LayoutInflater,
            liveData: MutableLiveData<State>
        ): BaseViewHolder<*>

        fun viewType(): Int = ordinal
    }

    sealed class State {
        data class OnAllClick(val serviceResponseItem: ArrayList<ServiceResponseItem>, val type: String? = "") : State()
    }

}