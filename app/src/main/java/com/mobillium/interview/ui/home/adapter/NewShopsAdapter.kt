package com.mobillium.interview.ui.home.adapter

import android.view.View
import com.bumptech.glide.Glide
import com.mobillium.interview.R
import com.mobillium.interview.base.adapter.BaseAdapter
import com.mobillium.interview.databinding.CellNewShopsItemBinding
import com.mobillium.interview.network.response.Shop

class NewShopsAdapter (
    shops: ArrayList<Shop>
): BaseAdapter<CellNewShopsItemBinding, Shop>(R.layout.cell_new_shops_item) {

    init {
        items = shops
    }
    override fun bindView(binding: CellNewShopsItemBinding, item: Shop?, adapterPosition: Int) {
        binding.shop = item

        if (item?.cover != null) {
            Glide.with(binding.root.context).load(item.cover?.url).into(binding.cellNewShopsItemImageView)
        }else {
            Glide.with(binding.root.context).load(R.mipmap.ic_launcher_round).into(binding.cellNewShopsItemImageView)
        }

        if (item?.logo != null){
            binding.cellNewShopsItemNullLogoCardView.visibility = View.GONE
            Glide.with(binding.root.context).load(item.logo?.url).into(binding.cellNewShopsItemUserImageView)
        }else {
            binding.cellNewShopsItemNullLogoCardView.visibility = View.VISIBLE
            binding.cellNewShopsNullLogoTextView.text = item?.name?.substring(0,1)
        }
    }

}