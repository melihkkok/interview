package com.mobillium.interview.ui.home

import android.app.Activity
import android.content.Intent
import android.speech.RecognizerIntent
import com.mobillium.interview.R
import com.mobillium.interview.base.view.BaseActivity
import com.mobillium.interview.databinding.ActivityHomeBinding
import com.mobillium.interview.network.response.ServiceResponseItem
import com.mobillium.interview.network.response.enums.ListingType
import com.mobillium.interview.ui.collections.CollectionDetailActivity
import com.mobillium.interview.ui.home.adapter.HomeListAdapter
import com.mobillium.interview.ui.products.ProductsActivity
import com.mobillium.interview.ui.shop_detail.ShopDetailActivity
import com.mobillium.interview.util.extension.observe
import com.mobillium.interview.util.extension.onSwipe

class HomeActivity(
    override val layoutResourceId: Int = R.layout.activity_home,
    override val classTypeOfViewModel: Class<HomeViewModel> = HomeViewModel::class.java
) : BaseActivity<ActivityHomeBinding, HomeViewModel>() {

    private val REQUEST_SEARCH_VOICE_ACTION = 1001
    lateinit var adapter: HomeListAdapter

    override fun initialize() {
        adapter = HomeListAdapter(
            serviceResponseItem = viewModel.serviceResponse!!
        )
        binding.homeRecyclerView.adapter = adapter

        binding.activityHomeSwipeRefreshLayout onSwipe {
            viewModel.getProducts()
        }

        binding.mic.setOnClickListener {
            openMic()
        }
        observe(viewModel.liveData, ::onStateChanged)
        observe(adapter.liveData, ::onAdapterStateChange)
    }

    private fun openMic(){
        try {
            val intent = Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH)
            intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM)
            startActivityForResult(intent, REQUEST_SEARCH_VOICE_ACTION)
        } catch (_: Exception) {
        }
    }

    private fun onAdapterStateChange(state: HomeListAdapter.State) {
        when (state) {
            is HomeListAdapter.State.OnAllClick -> goToAllProducts(
                state.serviceResponseItem,
                state.type
            )
        }
    }

    private fun onStateChanged(state: HomeViewModel.State) {
        when (state) {
            is HomeViewModel.State.onLoadingState -> {
                adapter = HomeListAdapter(state.serviceResponse)
                binding.homeRecyclerView.adapter = adapter
                binding.homeRecyclerView.adapter!!.notifyDataSetChanged()
                observe(adapter.liveData, ::onAdapterStateChange)
            }
        }
    }

    private fun goToAllProducts(serviceResponse: ArrayList<ServiceResponseItem>, type: String?) {
        when (type) {
            ListingType.TYPE_NEW_PRODUCTS.type -> ProductsActivity.start(this, serviceResponse)

            ListingType.TYPE_COLLECTIONS.type -> CollectionDetailActivity.start(
                this,
                serviceResponse
            )
            ListingType.TYPE_EDITOR_SHOPS.type -> ShopDetailActivity.start(
                this,
                serviceResponse,
                type
            )
            ListingType.TYPE_NEW_SHOPS.type -> ShopDetailActivity.start(this, serviceResponse, type)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_SEARCH_VOICE_ACTION && resultCode == Activity.RESULT_OK) {
            val matches = data?.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS) ?: ArrayList()
            if (matches.size != 0) {
                binding.search.setText(matches[0])
                binding.search.setSelection(binding.search.text.length)
            }
        }
    }

    companion object {
        const val EXTRA_SERVICE_RESPONSE = "com.mobillium.interview.ui.home_extra_service_response"
        fun start(activity: Activity, serviceResponse: ArrayList<ServiceResponseItem>) {
            activity.startActivity(Intent(activity, HomeActivity::class.java).apply {
                putParcelableArrayListExtra(EXTRA_SERVICE_RESPONSE, serviceResponse)
            })
        }
    }
}
