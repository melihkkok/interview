package com.mobillium.interview.ui.home.view_holder

import android.view.View
import com.mobillium.interview.base.adapter.BaseViewHolder
import com.mobillium.interview.databinding.CellCategoriesItemBinding
import com.mobillium.interview.databinding.CellHorizantalViewBinding
import com.mobillium.interview.network.response.ServiceResponseItem
import com.mobillium.interview.ui.home.adapter.CategoryAdapter

class CategoriesViewHolder(
    val binding: CellHorizantalViewBinding
) : BaseViewHolder<ArrayList<ServiceResponseItem>>(binding.root) {

    override fun bind(data: ArrayList<ServiceResponseItem>) {
        binding.cellHorizantalViewRecyclerView.adapter = CategoryAdapter(data[2].categories!!)
        binding.cellHorizantalViewTitleTextView.text = data[2].title
        binding.cellHorizantalViewAllTextView.visibility = View.GONE
    }

}