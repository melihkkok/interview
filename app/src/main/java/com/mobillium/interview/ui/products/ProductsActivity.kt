package com.mobillium.interview.ui.products

import android.app.Activity
import android.content.Intent
import androidx.recyclerview.widget.GridLayoutManager
import com.mobillium.interview.R
import com.mobillium.interview.base.view.BaseActivity
import com.mobillium.interview.databinding.ActivityProductsBinding
import com.mobillium.interview.network.response.ServiceResponseItem
import com.mobillium.interview.ui.products.adapter.ProductDetailAdapter

class ProductsActivity(
    override val layoutResourceId: Int = R.layout.activity_products,
    override val classTypeOfViewModel: Class<ProductsViewModel> = ProductsViewModel::class.java
) : BaseActivity<ActivityProductsBinding, ProductsViewModel>() {

    override fun initialize() {

        binding.title = viewModel.serviceResponse!![1].title

        val adapter = ProductDetailAdapter(
            viewModel.serviceResponse!![1].products!!
        )

        binding.activityProductsRecyclerView.layoutManager = GridLayoutManager(this, 2)
        binding.activityProductsRecyclerView.adapter = adapter

        binding.productDetailToolbarBackButton.setOnClickListener {
            onBackPressed()
        }
    }

    companion object {
        const val EXTRA_SERVICE_ITEM = "com.mobillium.interview.ui.products.extra_service_item"
        fun start(activity: Activity, serviceResponseItem: ArrayList<ServiceResponseItem>) {
            activity.startActivity(Intent(activity, ProductsActivity::class.java).apply {
                putParcelableArrayListExtra(EXTRA_SERVICE_ITEM, serviceResponseItem)
            })
        }
    }
}
