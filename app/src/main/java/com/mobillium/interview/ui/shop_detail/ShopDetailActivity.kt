package com.mobillium.interview.ui.shop_detail

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.mobillium.interview.R
import com.mobillium.interview.base.view.BaseActivity
import com.mobillium.interview.databinding.ActivityShopDetailBinding
import com.mobillium.interview.network.response.ServiceResponseItem
import com.mobillium.interview.network.response.enums.ListingType
import com.mobillium.interview.ui.shop_detail.adapter.ShopDetailAdapter

class ShopDetailActivity (
    override val layoutResourceId: Int = R.layout.activity_shop_detail,
    override val classTypeOfViewModel: Class<ShopDetailViewModel> = ShopDetailViewModel::class.java
) : BaseActivity<ActivityShopDetailBinding, ShopDetailViewModel>() {

    lateinit var adapter: ShopDetailAdapter

    override fun initialize() {

        if (viewModel.type.equals("editor_shops")){
            binding.title = viewModel.serviceResponse!![4].title
            adapter = ShopDetailAdapter(
                viewModel.serviceResponse!![4].shops!!
            )
        }else {
            binding.title = viewModel.serviceResponse!![5].title
            adapter = ShopDetailAdapter(
                viewModel.serviceResponse!![5].shops!!
            )
        }

        binding.activityShopRecyclerView.adapter = adapter

        binding.shopDetailToolbarBackButton.setOnClickListener {
            onBackPressed()
        }
    }

    companion object{
        const val EXTRA_SERVICE_ITEM = "com.mobillium.interview.ui.shop_detail.extra_service_item"
        fun start(activity: Activity, serviceResponseItem: ArrayList<ServiceResponseItem>, type: String) {
            activity.startActivity(Intent(activity, ShopDetailActivity::class.java).apply {
                putParcelableArrayListExtra(EXTRA_SERVICE_ITEM, serviceResponseItem)
                putExtra("type", type)
            })
        }
    }

}
