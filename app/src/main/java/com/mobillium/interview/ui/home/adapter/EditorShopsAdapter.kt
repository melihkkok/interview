package com.mobillium.interview.ui.home.adapter

import com.mobillium.interview.R
import com.mobillium.interview.base.adapter.BaseAdapter
import com.mobillium.interview.databinding.CellEditorShopsItemBinding
import com.mobillium.interview.network.response.Shop


class EditorShopsAdapter(
    shops: ArrayList<Shop>
) : BaseAdapter<CellEditorShopsItemBinding, Shop>(R.layout.cell_editor_shops_item) {

    init {
        items = shops
    }

    override fun bindView(binding: CellEditorShopsItemBinding, item: Shop?, adapterPosition: Int) {
        binding.isLastPosition = adapterPosition == (items!!.size - 1)
        binding.editorShops = item
        binding.cover = item?.cover
        binding.logo = item?.logo

        binding.cellEditorShopsPopularProductRecyclerView.adapter =
            EditorShopsProductAdapter(items!![adapterPosition].popular_products!!)
    }

}