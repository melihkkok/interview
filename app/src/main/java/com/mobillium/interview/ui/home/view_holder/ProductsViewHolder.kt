package com.mobillium.interview.ui.home.view_holder

import androidx.lifecycle.MutableLiveData
import com.mobillium.interview.base.adapter.BaseViewHolder
import com.mobillium.interview.databinding.CellHorizantalViewBinding
import com.mobillium.interview.network.response.ServiceResponseItem
import com.mobillium.interview.ui.home.adapter.HomeListAdapter
import com.mobillium.interview.ui.home.adapter.ProductAdapter

class ProductsViewHolder(
    val binding: CellHorizantalViewBinding,
    val liveData: MutableLiveData<HomeListAdapter.State>
) : BaseViewHolder<ArrayList<ServiceResponseItem>>(binding.root) {
    override fun bind(data: ArrayList<ServiceResponseItem>) {
        binding.cellHorizantalViewRecyclerView.adapter = ProductAdapter(data[1].products!!)
        binding.cellHorizantalViewTitleTextView.text = data[1].title

        binding.cellHorizantalViewAllTextView.setOnClickListener {
            liveData.value = HomeListAdapter.State.OnAllClick(data, data[1].type)
        }
    }

}