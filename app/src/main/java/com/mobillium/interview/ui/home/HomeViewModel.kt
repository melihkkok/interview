package com.mobillium.interview.ui.home

import android.os.Bundle
import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.mobillium.interview.base.viewmodel.BaseViewModel
import com.mobillium.interview.network.ApiDataManager
import com.mobillium.interview.network.response.ServiceResponse
import com.mobillium.interview.network.response.ServiceResponseItem
import javax.inject.Inject

class HomeViewModel @Inject constructor(private val apiDataManager: ApiDataManager) : BaseViewModel() {

    var liveData = MutableLiveData<State>()
    var serviceResponse : ArrayList<ServiceResponseItem>? = null
    override fun handleIntent(extras: Bundle) {
        serviceResponse = extras.getParcelableArrayList(HomeActivity.EXTRA_SERVICE_RESPONSE) ?: arrayListOf()
    }

    fun getProducts() {
        apiDataManager.getProducts().sendRequest {
            liveData.value = State.onLoadingState(it)
            Log.e("responnnnnse", it.size.toString())
        }
    }

    sealed class State{
        data class onLoadingState(val serviceResponse: ArrayList<ServiceResponseItem>): State()
    }
}