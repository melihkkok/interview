package com.mobillium.interview.ui.home.view_holder

import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.LinearSnapHelper
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.mobillium.interview.base.adapter.BaseViewHolder
import com.mobillium.interview.databinding.CellEditorShopsViewPagerItemBinding
import com.mobillium.interview.network.response.ServiceResponseItem
import com.mobillium.interview.ui.home.adapter.EditorShopsAdapter
import com.mobillium.interview.ui.home.adapter.HomeListAdapter


class EditorShopsViewHolder(
    val binding: CellEditorShopsViewPagerItemBinding,
    val liveData: MutableLiveData<HomeListAdapter.State>
) : BaseViewHolder<ArrayList<ServiceResponseItem>>(binding.root) {

    override fun bind(data: ArrayList<ServiceResponseItem>) {

        val snapHelper = LinearSnapHelper()
        // val adapter = EditorShopPagerAdapter(binding.root.context, data[4].shops!!)
        snapHelper.attachToRecyclerView(binding.cellEditorShopsRecyclerView)
        binding.cellEditorShopsRecyclerView.adapter = EditorShopsAdapter(data[4].shops!!)
        binding.cellEditorShopsTitleTextView.text = data[4].title

        Glide.with(binding.root.context).load(data[4].shops!![0].cover?.url.toString())
            .into(binding.cellEditorShopsBackgroundImageView)
        binding.cellEditorShopsRecyclerView.addOnScrollListener(object :
            RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                when (newState) {
                    RecyclerView.SCROLL_STATE_IDLE -> {
                        val positionView =
                            (recyclerView.layoutManager as LinearLayoutManager?)!!.findFirstCompletelyVisibleItemPosition()
                        if (positionView > -1)
                            Glide.with(binding.root.context)
                                .load(data[4].shops!![positionView].cover?.url.toString())
                                .into(binding.cellEditorShopsBackgroundImageView)
                    }
                }
            }
        })

        binding.cellEditorShopsAllTextView.setOnClickListener {
            liveData.value = HomeListAdapter.State.OnAllClick(data, data[4].type)
        }

    }

}