package com.mobillium.interview.ui.home.view_holder

import androidx.lifecycle.MutableLiveData
import com.mobillium.interview.base.adapter.BaseViewHolder
import com.mobillium.interview.databinding.CellHorizantalViewBinding
import com.mobillium.interview.network.response.ServiceResponseItem
import com.mobillium.interview.ui.home.adapter.CollectionsAdapter
import com.mobillium.interview.ui.home.adapter.HomeListAdapter


class CollectionsViewHolder(
    val binding: CellHorizantalViewBinding,
    val liveData: MutableLiveData<HomeListAdapter.State>
): BaseViewHolder<ArrayList<ServiceResponseItem>>(binding.root){
    override fun bind(data: ArrayList<ServiceResponseItem>) {
        binding.cellHorizantalViewRecyclerView.adapter = CollectionsAdapter(data[3].collections!!)
        binding.cellHorizantalViewTitleTextView.text = data[3].title

        binding.cellHorizantalViewAllTextView.setOnClickListener {
            liveData.value = HomeListAdapter.State.OnAllClick(data, data[3].type)
        }
    }

}