package com.mobillium.interview.ui.splash

import android.os.Bundle
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.mobillium.interview.base.viewmodel.BaseViewModel
import com.mobillium.interview.network.ApiDataManager
import com.mobillium.interview.network.response.ServiceResponse
import com.mobillium.interview.network.response.ServiceResponseItem
import javax.inject.Inject

class SplashViewModel @Inject constructor(private val apiDataManager: ApiDataManager) : BaseViewModel() {

    var liveData = MutableLiveData<State>()

    fun getProducts() {
        apiDataManager.getProducts().sendRequest {
            liveData.value = State.onLoadingState(it)
        }
    }

    sealed class State{
        data class onLoadingState(val serviceResponse: ArrayList<ServiceResponseItem>): State()
    }
}