package com.mobillium.interview.ui.home.adapter

import com.mobillium.interview.R
import com.mobillium.interview.base.adapter.BaseAdapter
import com.mobillium.interview.databinding.CellCategoriesItemBinding
import com.mobillium.interview.network.response.Category

class CategoryAdapter(
    products: ArrayList<Category>
) : BaseAdapter<CellCategoriesItemBinding, Category>(R.layout.cell_categories_item) {

    init {
        items = products
    }

    override fun bindView(
        binding: CellCategoriesItemBinding,
        item: Category?,
        adapterPosition: Int
    ) {
        binding.category = item
        binding.image = item!!.logo!!
    }

}

