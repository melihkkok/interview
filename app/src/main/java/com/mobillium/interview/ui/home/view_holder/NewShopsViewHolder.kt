package com.mobillium.interview.ui.home.view_holder

import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.LinearSnapHelper
import com.mobillium.interview.base.adapter.BaseViewHolder
import com.mobillium.interview.databinding.CellHorizantalViewBinding
import com.mobillium.interview.network.response.ServiceResponseItem
import com.mobillium.interview.ui.home.adapter.HomeListAdapter
import com.mobillium.interview.ui.home.adapter.NewShopsAdapter

class NewShopsViewHolder(
    val binding: CellHorizantalViewBinding,
    val liveData: MutableLiveData<HomeListAdapter.State>
) : BaseViewHolder<ArrayList<ServiceResponseItem>>(binding.root) {

    override fun bind(data: ArrayList<ServiceResponseItem>) {
        binding.cellHorizantalViewTitleTextView.text = data[5].title

        val snapHelper = LinearSnapHelper()
        snapHelper.attachToRecyclerView(binding.cellHorizantalViewRecyclerView)
        binding.cellHorizantalViewRecyclerView.adapter = NewShopsAdapter(data[5].shops!!)

        binding.cellHorizantalViewAllTextView.setOnClickListener {
            liveData.value = HomeListAdapter.State.OnAllClick(data, data[5].type)
        }
    }

}