package com.mobillium.interview.ui.collections.adapter

import com.mobillium.interview.R
import com.mobillium.interview.base.adapter.BaseAdapter
import com.mobillium.interview.databinding.CellCollectionDetailItemBinding
import com.mobillium.interview.network.response.Collection

class CollectionDetailAdapter (
    collections: ArrayList<Collection>
): BaseAdapter<CellCollectionDetailItemBinding, Collection>(R.layout.cell_collection_detail_item) {

    init {
        items = collections
    }

    override fun bindView(binding: CellCollectionDetailItemBinding, item: Collection?, adapterPosition: Int) {
        binding.collections = item
        binding.image = item!!.cover
    }

}