package com.mobillium.interview.ui.shop_detail.adapter

import android.view.View
import com.bumptech.glide.Glide
import com.mobillium.interview.R
import com.mobillium.interview.base.adapter.BaseAdapter
import com.mobillium.interview.databinding.CellShopDetailItemBinding
import com.mobillium.interview.network.response.Shop

class ShopDetailAdapter (
    shops: ArrayList<Shop>
): BaseAdapter<CellShopDetailItemBinding, Shop>(R.layout.cell_shop_detail_item) {

    init {
        items = shops
    }
    override fun bindView(binding: CellShopDetailItemBinding, item: Shop?, adapterPosition: Int) {
        binding.shop = item

        if (item?.cover != null) {
            Glide.with(binding.root.context).load(item.cover?.url).into(binding.activityShopsItemImageView)
        }else {
            Glide.with(binding.root.context).load(R.mipmap.ic_launcher_round).into(binding.activityShopsItemImageView)
        }

        if (item?.logo != null){
            binding.activityShopsItemNullLogoCardView.visibility = View.GONE
            Glide.with(binding.root.context).load(item.logo?.url).into(binding.activityShopsItemUserImageView)
        }else {
            binding.activityShopsItemNullLogoCardView.visibility = View.VISIBLE
            binding.activityShopsNullLogoTextView.text = item?.name?.substring(0,1)
        }
    }

}