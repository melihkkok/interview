package com.mobillium.interview.ui.home.adapter

import com.mobillium.interview.R
import com.mobillium.interview.base.adapter.BaseAdapter
import com.mobillium.interview.databinding.CellCollectionsItemBinding
import com.mobillium.interview.network.response.Collection


class CollectionsAdapter (
    collections: ArrayList<Collection>
): BaseAdapter<CellCollectionsItemBinding, Collection>(R.layout.cell_collections_item) {

    init {
        items = collections
    }

    override fun bindView(binding: CellCollectionsItemBinding, item: Collection?, adapterPosition: Int) {
        binding.collections = item
        binding.image = item!!.cover
    }

}