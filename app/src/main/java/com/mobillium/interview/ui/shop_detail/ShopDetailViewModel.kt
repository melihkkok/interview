package com.mobillium.interview.ui.shop_detail

import android.os.Bundle
import com.mobillium.interview.base.viewmodel.BaseViewModel
import com.mobillium.interview.network.response.ServiceResponseItem
import com.mobillium.interview.network.response.enums.ListingType
import javax.inject.Inject

class ShopDetailViewModel @Inject constructor() : BaseViewModel() {
    var serviceResponse : ArrayList<ServiceResponseItem>? = null
    var type: String? = ListingType.TYPE_EDITOR_SHOPS.type
    override fun handleIntent(extras: Bundle) {
        type = extras.getString("type") ?: ListingType.TYPE_EDITOR_SHOPS.type
        serviceResponse = extras.getParcelableArrayList(ShopDetailActivity.EXTRA_SERVICE_ITEM) ?: arrayListOf()
    }
}