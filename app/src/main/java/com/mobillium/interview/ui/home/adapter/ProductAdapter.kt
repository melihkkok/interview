package com.mobillium.interview.ui.home.adapter

import android.view.View
import android.view.animation.AnimationUtils
import com.mobillium.interview.R
import com.mobillium.interview.base.adapter.BaseAdapter
import com.mobillium.interview.databinding.CellProductsItemBinding
import com.mobillium.interview.network.response.Product

class ProductAdapter (
    products: ArrayList<Product>
): BaseAdapter<CellProductsItemBinding, Product>(R.layout.cell_products_item) {

    init {
        items = products
    }

    override fun bindView(binding: CellProductsItemBinding, item: Product?, adapterPosition: Int) {
        binding.product = item
        binding.image = item!!.images!![0]

    }

}

