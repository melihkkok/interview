package com.mobillium.interview.ui.collections

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import com.mobillium.interview.R
import com.mobillium.interview.base.view.BaseActivity
import com.mobillium.interview.databinding.ActivityCollectionDetailBinding
import com.mobillium.interview.network.response.ServiceResponseItem
import com.mobillium.interview.ui.collections.adapter.CollectionDetailAdapter
import com.mobillium.interview.ui.home.adapter.CollectionsAdapter

class CollectionDetailActivity (
    override val layoutResourceId: Int = R.layout.activity_collection_detail,
    override val classTypeOfViewModel: Class<CollectionDetailViewModel> = CollectionDetailViewModel::class.java
) : BaseActivity<ActivityCollectionDetailBinding, CollectionDetailViewModel>()  {

    override fun initialize() {
        binding.title = viewModel.serviceResponse!![3].title

        val adapter = CollectionDetailAdapter(
            viewModel.serviceResponse!![3].collections!!
        )
        binding.activityCollectionRecyclerView.adapter = adapter

        binding.collectionDetailToolbarBackButton.setOnClickListener {
            onBackPressed()
        }
    }

    companion object {
        const val EXTRA_SERVICE_ITEM = "com.mobillium.interview.ui.collections.extra_service_item"
        fun start(activity: Activity, serviceResponseItem: ArrayList<ServiceResponseItem>) {
            activity.startActivity(Intent(activity, CollectionDetailActivity::class.java).apply {
                putParcelableArrayListExtra(EXTRA_SERVICE_ITEM, serviceResponseItem)
            })
        }
    }

}
