package com.mobillium.interview.ui.home.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.viewpager.widget.PagerAdapter
import com.mobillium.interview.R
import com.mobillium.interview.databinding.SliderItemBinding
import com.mobillium.interview.network.response.Featured
import com.mobillium.interview.network.response.ServiceResponseItem

class HomePagerAdapter (
    var context: Context,
    var slider: ArrayList<ServiceResponseItem>
) : PagerAdapter() {

    var inflater: LayoutInflater = LayoutInflater.from(context)

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == `object`
    }

    override fun getCount(): Int {
        return slider[0].featured!!.size
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val binding  = DataBindingUtil.inflate<SliderItemBinding>(
            inflater,
            R.layout.slider_item,
            container,false
        )

        binding.slider = slider[0].featured!![position]
        container.addView(binding.root)


        return binding.root
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View)
    }
}