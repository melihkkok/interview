package com.mobillium.interview.ui.home.adapter

import com.mobillium.interview.R
import com.mobillium.interview.base.adapter.BaseAdapter
import com.mobillium.interview.databinding.CellEditorShopsProductItemBinding
import com.mobillium.interview.network.response.PopularProduct

class EditorShopsProductAdapter(
    popularProduct: ArrayList<PopularProduct>
) : BaseAdapter<CellEditorShopsProductItemBinding, PopularProduct>(R.layout.cell_editor_shops_product_item) {

    init {
        items = popularProduct
    }

    override fun bindView(
        binding: CellEditorShopsProductItemBinding,
        item: PopularProduct?,
        adapterPosition: Int
    ) {
        binding.image = item?.images!![0]

    }

}