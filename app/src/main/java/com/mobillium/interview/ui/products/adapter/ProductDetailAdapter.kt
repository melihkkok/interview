package com.mobillium.interview.ui.products.adapter

import com.mobillium.interview.R
import com.mobillium.interview.base.adapter.BaseAdapter
import com.mobillium.interview.databinding.CellProductDetailItemBinding
import com.mobillium.interview.network.response.Product

class ProductDetailAdapter(
    products: ArrayList<Product>
) : BaseAdapter<CellProductDetailItemBinding, Product>(R.layout.cell_product_detail_item) {

    init {
        items = products
    }

    override fun bindView(
        binding: CellProductDetailItemBinding,
        item: Product?,
        adapterPosition: Int
    ) {
        binding.product = item
        binding.image = item!!.images!![0]
    }
}
