package com.mobillium.interview.ui.splash

import com.mobillium.interview.R
import com.mobillium.interview.base.view.BaseActivity
import com.mobillium.interview.databinding.ActivitySplashBinding
import com.mobillium.interview.ui.home.HomeActivity
import com.mobillium.interview.util.extension.observe

class SplashActivity(
    override val layoutResourceId: Int = R.layout.activity_splash,
    override val classTypeOfViewModel: Class<SplashViewModel> = SplashViewModel::class.java
) : BaseActivity<ActivitySplashBinding, SplashViewModel>() {

    override fun initialize() {
        observe(viewModel.liveData, ::onStateChanged)
    }

    override fun initStartRequest() {
        viewModel.getProducts()
    }

    private fun onStateChanged(state: SplashViewModel.State) {
        when (state) {
            is SplashViewModel.State.onLoadingState -> {
                binding.splashAnimation.pauseAnimation()
                HomeActivity.start(this, state.serviceResponse)
            }
        }
    }
}
