package com.mobillium.interview.base.view

import android.app.Dialog
import android.os.Bundle
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.ViewModelProvider
import com.afollestad.materialdialogs.MaterialDialog
import com.mobillium.interview.base.viewmodel.BaseViewModel
import com.mobillium.interview.util.extension.getErrorMessage
import com.mobillium.interview.util.extension.observe
import dagger.android.AndroidInjection
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import javax.inject.Inject

abstract class BaseActivity<DB : ViewDataBinding, VM : BaseViewModel> : AppCompatActivity(),
    HasAndroidInjector {
    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Any>

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    override fun androidInjector() = dispatchingAndroidInjector

    @get:LayoutRes
    protected abstract val layoutResourceId: Int
    lateinit var binding: DB
    lateinit var viewModel: VM
    protected abstract val classTypeOfViewModel: Class<VM>
    private var hasRequestSend = false
    private var progressDialog: Dialog? = null

    private var hasInjected: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (hasInjected.not()) {
            AndroidInjection.inject(this)
            hasInjected = true
        }

        viewModel = ViewModelProvider(this, viewModelFactory).get(classTypeOfViewModel)
        binding = DataBindingUtil.setContentView(this, layoutResourceId)

        intent?.extras?.let {
            viewModel.handleIntent(it)
            it.clear()
        }

        setUpViewModelStateObservers()

        initialize()

        if (!hasRequestSend) {
            initStartRequest()
            hasRequestSend = true
        }
    }

    open fun initStartRequest() {}
    open fun initialize() {}

    private fun setUpViewModelStateObservers() {
        observe(viewModel.baseLiveData, ::onStateChanged)
    }

    private fun onStateChanged(state: BaseViewModel.State) {
        when (state) {
            is BaseViewModel.State.OnError -> showError(state.throwable)
        }
    }

    private fun showError(throwable: Throwable) {
        val dialogMessage = throwable.getErrorMessage(this)
        generateInitRequestError(dialogMessage)

    }

    private fun generateInitRequestError(dialogMessage: String) {
        MaterialDialog(this).show {
            message(text = dialogMessage)
            cancelable(false)
            positiveButton(text = "Yeniden Dene") { initStartRequest() }
            negativeButton(text = "Kapat") { onBackPressed() }
        }
    }

}
