package com.mobillium.interview

import com.mobillium.interview.di.component.DaggerAppComponent
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication

class MobilliumApp : DaggerApplication() {

    override fun onCreate() {
        super.onCreate()
    }

    override fun applicationInjector(): AndroidInjector<out MobilliumApp> {
        return DaggerAppComponent.builder().application(this).build()
    }

}